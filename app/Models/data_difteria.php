<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class data_difteria extends Model
{
    use HasFactory;

    protected $fillable = [
        'kecamatan','jml_kepadatan','jml_rumahtdksehat','jml_vaksin_dpt','jml_kasus','tahun',
    ];
}
