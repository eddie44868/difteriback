<?php
namespace App\Http\Controllers;
use App\models\data_difteria;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DifteriaWebController extends Controller
{
    
    public function index()
    {
        /// mengambil data terakhir dan pagination 5 list
        $posts = data_difteria::orderBy('tahun')->paginate(10);
         
        /// mengirimkan variabel $posts ke halaman views posts/index.blade.php
        /// include dengan number index
        return view('data_difterias.index',compact('posts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
  
    public function create()
    {
        /// menampilkan halaman create
        return view('data_difterias.create');
    }
  
    public function store(Request $request)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $validated = $request->validate([
            'kecamatan' => 'required',
            'jml_kepadatan' => 'required',
            'jml_rumahtdksehat' => 'required',
            'jml_vaksin_dpt' => 'required',
            'jml_kasus' => 'required',
            'tahun' => 'required',
        ]);
         
        $maxId = data_difteria::query()->max('id');
        
        $validated['id'] = $maxId+1;
        /// insert setiap request dari form ke dalam database via model
        /// jika menggunakan metode ini, maka nama field dan nama form harus sama
        data_difteria::insert($validated);
         
        /// redirect jika sukses menyimpan data
        return redirect()->route('difteria.index')
                        ->with('success','Post created successfully.');
    }
  
    public function show(data_difteria $difterium)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('posts.show',$post->id) }}
        return view('data_difterias.show',compact('post'));
    }
  
    public function edit(data_difteria $difterium)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('posts.edit',$post->id) }}
        return view('data_difterias.edit',compact('difterium'));
    }
  
    public function update(Request $request, data_difteria $difterium)
    {
        /// membuat validasi untuk title dan content wajib diisi
        $request->validate([
            'kecamatan' => 'required',
            'jml_kepadatan' => 'required',
            'jml_rumahtdksehat' => 'required',
            'jml_vaksin_dpt' => 'required',
            'jml_kasus' => 'required',
            'tahun' => 'required',
        ]);
         
        /// mengubah data berdasarkan request dan parameter yang dikirimkan
        $difterium->update($request->all());
         
        /// setelah berhasil mengubah data
        return redirect()->route('difteria.index')
                        ->with('success','Post updated successfully');
    }
  
    public function destroy(data_difteria $difterium)
    {
        /// melakukan hapus data berdasarkan parameter yang dikirimkan
        $difterium->delete();
  
        return redirect()->route('difteria.index')
                        ->with('success','Post deleted successfully');
    }

    public function search(Request $request){
        $search = $request->get('search');
        $post = data_difteria::orderBy('tahun')->where('kecamatan', 'ilike', '%'.$search.'%')->paginate(5);
        return view('data_difterias.index', ['posts' => $post]);

    }
}
