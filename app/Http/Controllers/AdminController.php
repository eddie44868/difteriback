<?php

namespace App\Http\Controllers;
use App\models\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){

        return admin::all();
    }

    public function create(Request $request){
        $admin=new admin;
        $admin->uname=$request->uname;
        $admin->pass=$request->pass;
        $admin->nama_lengkap=$request->nama_lengkap;
        $admin->instansi=$request->instansi;
        $admin->save();

        return " Data Tersimpan ";
    }

    public function update(Request $request, $id){
        $admin=admin::find($id);
        $admin->uname=$request->uname;
        $admin->pass=$request->pass;
        $admin->nama_lengkap=$request->nama_lengkap;
        $admin->instansi=$request->instansi;
        $admin->save();

        return " Data Terupdate ";
    }

    public function delete($id){
        $admin=admin::find($id);
        $admin->delete();

        return " Data Terhapus ";
    }

    public function detail($id){
        $admin=admin::find($id);

        return $admin;
    }
}
