<?php

namespace App\Http\Controllers;
use App\models\data_difteria;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DataDifteriaController extends Controller
{
    public function index(){

        return data_difteria::all();
    }

    public function create(Request $request){
        $data=new data_difteria;
        $data->jml_kepadatan=$request->jml_kepadatan;
        $data->jml_rumahtdksehat=$request->jml_rumahtdksehat;
        $data->jml_vaksin_dpt=$request->jml_vaksin_dpt;
        $data->jml_penderita=$request->jml_penderita;
        $data->save();

        return " Data Tersimpan ";
    }

    public function update(Request $request, $id){
        $data=data_difteria::find($id);
        $data->jml_kepadatan=$request->jml_kepadatan;
        $data->jml_rumahtdksehat=$request->jml_rumahtdksehat;
        $data->jml_vaksin_dpt=$request->jml_vaksin_dpt;
        $data->jml_penderita=$request->jml_penderita;
        $data->save();

        return " Data Terupdate ";
    }

    public function delete($id){
        $data=data_difteria::find($id);
        $data->delete();

        return " Data Terhapus ";
    }

    public function detail($id){
        $data=data_difteria::find($id);

        return $data;
    }

    public function detail2016(){
        $data = data_difteria::where('tahun', 2016)->get();
        return $data;
    }

    public function rendah2016(){
        $data = data_difteria::where('tahun', 2016)->where('cluster', 'C1')->get();
        return $data;
    }

    public function sedang2016(){
        $data = data_difteria::where('tahun', 2016)->where('cluster', 'C2')->get();
        return $data;
    }

    public function tinggi2016(){
        $data = data_difteria::where('tahun', 2016)->where('cluster', 'C3')->get();
        return $data;
    }


    public function detail2017(){
        $data = data_difteria::where('tahun', 2017)->get();
        return $data;
    }


    public function rendah2017(){
        $data = data_difteria::where('tahun', 2017)->where('cluster', 'C1')->get();
        return $data;
    }

    public function sedang2017(){
        $data = data_difteria::where('tahun', 2017)->where('cluster', 'C2')->get();
        return $data;
    }

    public function tinggi2017(){
        $data = data_difteria::where('tahun', 2017)->where('cluster', 'C3')->get();
        return $data;
    }


    public function detail2018(){
        $data = data_difteria::where('tahun', 2018)->get();
        return $data;
    }
    
    public function rendah2018(){
        $data = data_difteria::where('tahun', 2018)->where('cluster', 'C1')->get();
        return $data;
    }

    public function sedang2018(){
        $data = data_difteria::where('tahun', 2018)->where('cluster', 'C2')->get();
        return $data;
    }

    public function tinggi2018(){
        $data = data_difteria::where('tahun', 2018)->where('cluster', 'C3')->get();
        return $data;
    }

    public function detail2019(){
        $data = data_difteria::where('tahun', 2019)->get();
        return $data;
    }

    
    public function rendah2019(){
        $data = data_difteria::where('tahun', 2019)->where('cluster', 'C1')->get();
        return $data;
    }

    public function sedang2019(){
        $data = data_difteria::where('tahun', 2019)->where('cluster', 'C2')->get();
        return $data;
    }

    public function tinggi2019(){
        $data = data_difteria::where('tahun', 2019)->where('cluster', 'C3')->get();
        return $data;
    }

    public function detail2020(){
        $data = data_difteria::where('tahun', 2020)->get();
        return $data;
    }

    
    public function rendah2020(){
        $data = data_difteria::where('tahun', 2020)->where('cluster', 'C1')->get();
        return $data;
    }

    public function sedang2020(){
        $data = data_difteria::where('tahun', 2020)->where('cluster', 'C2')->get();
        return $data;
    }

    public function tinggi2020(){
        $data = data_difteria::where('tahun', 2020)->where('cluster', 'C3')->get();
        return $data;
    }
}
