<?php

namespace App\Http\Controllers;
use App\models\kecamatan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    public function index(){

        return kecamatan::all();
    }

    public function create(Request $request){
        $kecamatan=new kecamatan;
        $kecamatan->nama_kecamatan=$request->nama_kecamatan;
        $kecamatan->save();

        return " Data Tersimpan ";
    }

    public function update(Request $request, $id){
        $kecamatan=kecamatan::find($id);
        $kecamatan->nama_kecamatan=$request->nama_kecamatan;
        $kecamatan->save();

        return " Data Terupdate ";
    }

    public function delete($id){
        $kecamatan=kecamatan::find($id);
        $kecamatan->delete();

        return " Data Terhapus ";
    }

    public function detail($id){
        $kecamatan=kecamatan::find($id);

        return $kecamatan;
    }
}
