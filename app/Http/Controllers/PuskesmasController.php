<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\puskesmas;
use App\Http\Controllers\Controller;
class PuskesmasController extends Controller
{

    public function index(){

        return puskesmas::all();
    }

    public function create(Request $request){
        $puskesmas=new puskesmas;
        $puskesmas->telp_puskesmas=$request->telp_puskesmas;
        $puskesmas->alamat=$request->alamat;
        $puskesmas->kecamatan=$request->kecamatan;
        $puskesmas->save();

        return " Data Tersimpan ";
    }

    public function update(Request $request, $id){
        $puskesmas=puskesmas::find($id);
        $puskesmas->telp_puskesmas=$request->telp_puskesmas;
        $puskesmas->alamat=$request->alamat;
        $puskesmas->kecamatan=$request->kecamatan;
        $puskesmas->save();

        return " Data Terupdate ";
    }

    public function delete($id){
        $puskesmas=puskesmas::find($id);
        $puskesmas->delete();

        return " Data Terhapus ";
    }

    public function detail($id){
        $puskesmas=puskesmas::find($id);

        return $puskesmas;
    }
}
