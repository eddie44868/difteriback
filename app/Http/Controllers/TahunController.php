<?php

namespace App\Http\Controllers;
use App\models\tahun;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TahunController extends Controller
{
    public function index(){

        return tahun::all();
    }

    public function create(Request $request){
        $tahun=new tahun;
        $tahun->tahun_kejadian=$request->tahun_kejadian;
        $tahun->save();

        return " Data Tersimpan ";
    }

    public function update(Request $request, $id){
        $tahun=tahun::find($id);
        $tahun->tahun_kejadian=$request->tahun_kejadian;
        $tahun->save();

        return " Data Terupdate ";
    }

    public function delete($id){
        $tahun=tahun::find($id);
        $tahun->delete();

        return " Data Terhapus ";
    }

    public function detail($id){
        $tahun=tahun::find($id);

        return $tahun;
    }
}