<?php

namespace App\Http\Controllers;
use App\models\kmeans2020;
use App\models\data_difteria;
use App\Http\Controllers\Controller;

class Kmeans2020Controller extends Controller
{
    public function run(){
    $conn_string = "host=127.0.0.1 port=5432 dbname=mydb user=postgres password=breaker4816";
    $dbconn = pg_connect($conn_string);

    if (pg_last_error()) {
        echo "Tidak terhubung";
        exit;
    }

    function caridata($dbconn, $query)
    {

        $result = pg_query($dbconn, $query);
        $row = pg_fetch_array($result);
        // $row = $dbconn->query($query)->fetch_array();
        return $row[0];
    }

    //inisialisasi cluster awal
    $jmldata = caridata($dbconn, "SELECT Count(*) FROM data_difterias WHERE tahun='2020'");
    echo "jumlah data = $jmldata ";
    for ($i = 0; $i < $jmldata; $i++) {
        $clusterawal[$i] = "1";
    }

    //Set Default nilai centroid (kepadatan, rumah, vaksin, kasus)
    $centro1[0] = array('7725.14', '74', '945', '0');
    $centro2[0] = array('23166.9', '2622', '1188', '0');
    $centro3[0] = array('28711.4', '3496', '2490', '2');

    $status = 'false';
    $loop = '0';
    $x='0';
    while ($status == 'false') {
        $query = "select * from data_difterias where tahun='2020'";
        $result = pg_query($dbconn, $query);
        while ($data = pg_fetch_assoc($result)) {
            extract($data);
            $hasilc1 = 0;
            $hasilc2 = 0;
            $hasilc3 = 0;

            $hasilc1 = sqrt(
                pow($jml_kepadatan - $centro1[$loop][0], 2) +
                pow($jml_rumahtdksehat - $centro1[$loop][1], 2) +
                pow($jml_vaksin_dpt - $centro1[$loop][2], 2) +
                pow($jml_kasus - $centro1[$loop][3], 2)
            );

            $hasilc2 = sqrt(
                pow($jml_kepadatan - $centro2[$loop][0], 2) +
                pow($jml_rumahtdksehat - $centro2[$loop][1], 2) +
                pow($jml_vaksin_dpt - $centro2[$loop][2], 2) +
                pow($jml_kasus - $centro2[$loop][3], 2)
            );

            $hasilc3 = sqrt(
                pow($jml_kepadatan - $centro3[$loop][0], 2) +
                pow($jml_rumahtdksehat - $centro3[$loop][1], 2) +
                pow($jml_vaksin_dpt - $centro3[$loop][2], 2) +
                pow($jml_kasus - $centro3[$loop][3], 2)
            );

            if ($hasilc1<$hasilc2 && $hasilc1<$hasilc3) {
                $clusterakhir[$x]='C1';
                $query = "UPDATE data_difterias SET cluster = 'C1' WHERE id=$id";
                pg_query($dbconn, $query);
                //pg_update($dbconn, 'tb_mahasiswa', $set_sementara, $clusterakhir);
                //update_mahasiswa($dbconn,$id,'C1');

            }else if ($hasilc2<$hasilc1 && $hasilc2<$hasilc3) {
                $clusterakhir[$x]='C2';
                $query = "UPDATE data_difterias SET cluster = 'C2' WHERE id=$id";
                pg_query($dbconn, $query);
                //pg_update($dbconn, 'tb_mahasiswa', $set_sementara, $clusterakhir);
                //update_mahasiswa($dbconn,$id,'C2');
            }else {
                $clusterakhir[$x]='C3';
                $query = "UPDATE data_difterias SET cluster = 'C3' WHERE id=$id";
                pg_query($dbconn, $query);
                //pg_update($dbconn, 'tb_mahasiswa', $set_sementara, $clusterakhir);
                //update_mahasiswa($dbconn,$id,'C3');
            }

            $x+=1;
    
        }
        $loop+=1;
        //cari centroid baru
        //centroid baru pusat 1
        $centro1[$loop][0]=caridata($dbconn,"select avg(jml_kepadatan) from data_difterias where cluster='C1'");
        $centro1[$loop][1]=caridata($dbconn,"select avg(jml_rumahtdksehat) from data_difterias where cluster='C1'");
        $centro1[$loop][2]=caridata($dbconn,"select avg(jml_vaksin_dpt) from data_difterias where cluster='C1'");
        $centro1[$loop][3]=caridata($dbconn,"select avg(jml_kasus) from data_difterias where cluster='C1'");

        //Centroid Baru Pusat 2
        $centro2[$loop][0]=caridata($dbconn,"select avg(jml_kepadatan) from data_difterias where cluster='C2'");
        $centro2[$loop][1]=caridata($dbconn,"select avg(jml_rumahtdksehat) from data_difterias where cluster='C2'");
        $centro2[$loop][2]=caridata($dbconn,"select avg(jml_vaksin_dpt) from data_difterias where cluster='C2'");
        $centro2[$loop][3]=caridata($dbconn,"select avg(jml_kasus) from data_difterias where cluster='C2'");

        //Centroid Baru Pusat 3
        $centro3[$loop][0]=caridata($dbconn,"select avg(jml_kepadatan) from data_difterias where cluster='C3'");
        $centro3[$loop][1]=caridata($dbconn,"select avg(jml_rumahtdksehat) from data_difterias where cluster='C3'");
        $centro3[$loop][2]=caridata($dbconn,"select avg(jml_vaksin_dpt) from data_difterias where cluster='C3'");
        $centro3[$loop][3]=caridata($dbconn,"select avg(jml_kasus) from data_difterias where cluster='C3'");
        
        $status = 'true';
        
    for ($i=0; $i <$jmldata ; $i++) { 
        if ($clusterawal[$i]!=$clusterakhir[$i]) {
            $status='false';
        }
    }

    if ($status=='false') {
        $clusterawal=$clusterakhir;
    }

    }

    return "Proses berhasil dilakukan sebanyak $loop kali";
        }
    }
?>