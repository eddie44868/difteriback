<?php

namespace App\Imports;

use App\Models\data_difteria;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow; 

class DifteriaImport implements OnEachRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new data_difteria([
            
            'kecamatan' => $row[1],
            'jml_kepadatan' => $row[2],
            'jml_rumahtdksehat' => $row[3],
            'jml_vaksin_dpt' => $row[4],
            'jml_kasus' => $row[5],
            'tahun' => $row[6],

        ]);
    }

    public function onRow(Row $row)
    {
        $rowIndex = $row->getIndex();
        $row      = $row->toArray();
         
        $maxId = data_difteria::query()->max('id');
        
        data_difteria::query()->insert([
            'id' => $maxId+1,
            'kecamatan' => $row[1],
            'jml_kepadatan' => $row[2],
            'jml_rumahtdksehat' => $row[3],
            'jml_vaksin_dpt' => $row[4],
            'jml_kasus' => $row[5],
            'tahun' => $row[6],
        ]);
    }
}
