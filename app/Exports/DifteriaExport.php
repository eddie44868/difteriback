<?php

namespace App\Exports;

use App\Models\data_difteria;
use Maatwebsite\Excel\Concerns\FromCollection;


class DifteriaExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return data_difteria::all();
    }
}
