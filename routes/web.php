<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\DifteriaWebController;
use App\Http\Controllers\ExportImportController;
Route::resource('difteria', DifteriaWebController::class);
Route::resource('ExportImport', ExportImportController::class);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/create',[DifteriaWebController::class, 'create']);
// Route::delete('/{difteria}',[DifteriaWebController::class, 'destroy']);
Route::put('/update',[DifteriaWebController::class, 'update']);
Route::get('/search',[DifteriaWebController::class, 'search']);

Route::get('/export',[ExportImportController::class, 'export'])->name('export');
Route::get('/',[ExportImportController::class, 'index'])->name('index');
Route::post('/import',[ExportImportController::class, 'import'])->name('import');

