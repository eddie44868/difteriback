<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PuskesmasController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\TahunController;
use App\Http\Controllers\DataDifteriaController;
use App\Http\Controllers\KmeansController;
use App\Http\Controllers\Kmeans2017Controller;
use App\Http\Controllers\Kmeans2018Controller;
use App\Http\Controllers\Kmeans2019Controller;
use App\Http\Controllers\Kmeans2020Controller;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('puskesmas',[PuskesmasController::class, 'index']);
Route::post('puskesmas',[PuskesmasController::class, 'create']);
Route::get('/puskesmas/{id}',[PuskesmasController::class, 'detail']);
Route::put('/puskesmas/{id}',[PuskesmasController::class, 'update']);
Route::delete('/puskesmas/{id}',[PuskesmasController::class, 'delete']);

Route::get('admin',[AdminController::class, 'index']);
Route::post('admin',[AdminController::class, 'create']);
Route::get('/admin/{id}',[AdminController::class, 'detail']);
Route::put('/admin/{id}',[AdminController::class, 'update']);
Route::delete('admin/{id}',[AdminController::class, 'delete']);


Route::get('tahun',[TahunController::class, 'index']);
Route::post('tahun',[TahunController::class, 'create']);
Route::get('/tahun/{id}',[TahunController::class, 'detail']);
Route::put('/tahun/{id}',[TahunController::class, 'update']);
Route::delete('tahun/{id}',[TahunController::class, 'delete']);

// Route::get('kecamatan',[KecamatanController::class, 'index']);
// Route::post('kecamatan',[KecamatanController::class, 'create']);
// Route::get('/kecamatan/{id}',[KecamatanController::class, 'detail']);
// Route::put('/kecamatan/{id}',[KecamatanController::class, 'update']);
// Route::delete('kecamatan/{id}',[KecamatanControllerr::class, 'delete']);

Route::get('data_difteria',[DataDifteriaController::class, 'index']);
Route::post('data_difteria',[DataDifteriaController::class, 'create']);
Route::get('/data_difteria/{id}',[DataDifteriaController::class, 'detail']);
Route::put('/data_difteria/{id}',[DataDifteriaController::class, 'update']);
Route::delete('data_difteria/{id}',[DataDifteriaController::class, 'delete']);

Route::get('getData2016',[DataDifteriaController::class, 'detail2016']);
Route::get('getData2017',[DataDifteriaController::class, 'detail2017']);
Route::get('getData2018',[DataDifteriaController::class, 'detail2018']);
Route::get('getData2019',[DataDifteriaController::class, 'detail2019']);
Route::get('getData2020',[DataDifteriaController::class, 'detail2020']);

Route::get('cluster2016',[KmeansController::class, 'run']);
Route::get('cluster2017',[Kmeans2017Controller::class, 'run']);
Route::get('cluster2018',[Kmeans2018Controller::class, 'run']);
Route::get('cluster2019',[Kmeans2019Controller::class, 'run']);
Route::get('cluster2020',[Kmeans2020Controller::class, 'run']);

Route::get('rendah2016',[DataDifteriaController::class, 'rendah2016']);
Route::get('sedang2016',[DataDifteriaController::class, 'sedang2016']);
Route::get('tinggi2016',[DataDifteriaController::class, 'tinggi2016']);

Route::get('rendah2017',[DataDifteriaController::class, 'rendah2017']);
Route::get('sedang2017',[DataDifteriaController::class, 'sedang2017']);
Route::get('tinggi2017',[DataDifteriaController::class, 'tinggi2017']);

Route::get('rendah2018',[DataDifteriaController::class, 'rendah2018']);
Route::get('sedang2018',[DataDifteriaController::class, 'sedang2018']);
Route::get('tinggi2018',[DataDifteriaController::class, 'tinggi2018']);

Route::get('rendah2019',[DataDifteriaController::class, 'rendah2019']);
Route::get('sedang2019',[DataDifteriaController::class, 'sedang2019']);
Route::get('tinggi2019',[DataDifteriaController::class, 'tinggi2019']);

Route::get('rendah2020',[DataDifteriaController::class, 'rendah2020']);
Route::get('sedang2020',[DataDifteriaController::class, 'sedang2020']);
Route::get('tinggi2020',[DataDifteriaController::class, 'tinggi2020']);