<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataDifteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_difterias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kecamatan');
            $table->float('jml_kepadatan');
            $table->integer('jml_rumahtdksehat');
            $table->integer('jml_vaksin_dpt');
            $table->integer('jml_kasus');
            $table->integer('tahun');
            $table->string('cluster')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_difterias');
    }
}
