@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2> Export Import Data Difteria</h2>
        </div>
        <div class="float-lg-right" style="margin-right: 5px;">
            <a class="btn btn-secondary" href="{{ route('difteria.index') }}"> Back</a>
        </div>
        <div class="float-lg-right" style="margin-right: 5px;">
            <a class="btn btn-success" href="{{ route('export') }}">Export</a>
        </div>
        <div class="float-lg-right" style="margin-right: 5px;">
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Import</a>
        </div>

    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="card card-info card-outline">
    <div class="card-body">
        <table class="table table-bordered">
            <tr class="text-center">
                <th>Kecamatan</th>
                <th>Kepadatan</th>
                <th>Rumah Tidak Sehat</th>
                <th>Vaksin DPT</th>
                <th>Kasus Difteri</th>
                <th>Tahun</th>
            </tr>
            @foreach ($exim as $post)
            <tr class="text-center">
                <td>{{ $post->kecamatan }}</td>
                <td>{{ $post->jml_kepadatan }}</td>
                <td>{{ $post->jml_rumahtdksehat }}</td>
                <td>{{ $post->jml_vaksin_dpt }}</td>
                <td>{{ $post->jml_kasus }}</td>
                <td>{{ $post->tahun }}</td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
                    <div class="modal-body">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="file" name="file" required="required">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection