@extends('template')

@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2> Web Admin Difteria</h2>
        </div>
        <div class="col-md-4">
            <form action="/search" method="get">
                <div class="input-group">
                    <input type="search" name="search" class="form-control" style="margin-right: 10px;">
                    <span class="input-group-prepend">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </span>
                </div>
            </form>
        </div>
        <div class="float-right">
            <a class="btn btn-success" href="{{ route('difteria.create') }}"> Create</a>
        </div>
        <div class="float-lg-right" style="margin-right: 5px;">
            <a class="btn btn-success" href="{{ route('ExportImport.index') }}"> Export Import</a>
        </div>
        <div class="float-lg-right" style="margin-right: 5px;">
            <a class="btn btn-secondary" href="{{ route('difteria.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr class="text-center">
        <th>Kecamatan</th>
        <th>Kepadatan</th>
        <th>Rumah Tidak Sehat</th>
        <th>Vaksin DPT</th>
        <th>Kasus Difteri</th>
        <th>Tahun</th>
        <th width="280px" class="text-center">Action</th>
    </tr>
    @foreach ($posts as $post)
    <tr class="text-center">
        <td>{{ $post->kecamatan }}</td>
        <td>{{ $post->jml_kepadatan }}</td>
        <td>{{ $post->jml_rumahtdksehat }}</td>
        <td>{{ $post->jml_vaksin_dpt }}</td>
        <td>{{ $post->jml_kasus }}</td>
        <td>{{ $post->tahun }}</td>

        <td class="text-center">
            <form action="{{ route('difteria.destroy',$post->id) }}" method="POST">

                <a class="btn btn-primary btn-sm" href="{{ route('difteria.edit',$post->id) }}">Update</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $posts->links('pagination::bootstrap-4') !!}

@endsection