@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('difteria.index') }}"> Back</a>
            </div>
        </div>
    </div>
 
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 
    <form action="{{ route('difteria.update',$difterium->id) }}" method="POST">
        @csrf
        @method('PUT')
 
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Kecamatan</strong>
                    <input type="text" name="kecamatan" value="{{ $difterium->kecamatan }}" class="form-control" placeholder="kecamatan">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Kepadatan</strong>
                    <input type="number" name="jml_kepadatan" value="{{ $difterium->jml_kepadatan }}" class="form-control" placeholder="jml_kepadatan">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Rumah Tidak Sehat</strong>
                    <input type="number" name="jml_rumahtdksehat" value="{{ $difterium->jml_rumahtdksehat }}" class="form-control" placeholder="jml_rumahtdksehat">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Vaksin DPT</strong>
                    <input type="number" name="jml_vaksin_dpt" value="{{ $difterium->jml_vaksin_dpt }}" class="form-control" placeholder="jml_vaksin_dpt">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Kasus Difteri</strong>
                    <input type="number" name="jml_kasus" value="{{ $difterium->jml_kasus }}" class="form-control" placeholder="jml_kasus">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tahun</strong>
                    <input type="number" name="tahun" value="{{ $difterium->tahun }}" class="form-control" placeholder="tahun">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
 
    </form>
@endsection