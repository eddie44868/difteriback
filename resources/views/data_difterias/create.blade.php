@extends('template')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Data</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('difteria.index') }}"> Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('difteria.store') }}" method="POST">
    @csrf
 
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Kecamatan</strong>
                <input type="text" name="kecamatan" class="form-control" placeholder="kecamatan">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jumlah Kepadatan</strong>
                <input type="number" name="jml_kepadatan" class="form-control" placeholder="jml_kepadatan">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jumlah Rumah Tidak Sehat</strong>
                <input type="number" name="jml_rumahtdksehat" class="form-control" placeholder="jml_rumahtdksehat">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jumlah Vaksin DPT</strong>
                <input type="number" name="jml_vaksin_dpt" class="form-control" placeholder="jml_vaksin_dpt">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jumlah Kasus Difteri</strong>
                <input type="number" name="jml_kasus" class="form-control" placeholder="jml_kasus">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tahun</strong>
                <input type="number" name="tahun" class="form-control" placeholder="tahun">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
 
</form>
@endsection